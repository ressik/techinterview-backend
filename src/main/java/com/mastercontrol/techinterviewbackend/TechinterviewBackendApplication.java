package com.mastercontrol.techinterviewbackend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechinterviewBackendApplication {
    public static void main(String[] args) {
        SpringApplication.run(TechinterviewBackendApplication.class, args);
    }

}
