package com.mastercontrol.techinterviewbackend.repository;

import com.mastercontrol.techinterviewbackend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

}
